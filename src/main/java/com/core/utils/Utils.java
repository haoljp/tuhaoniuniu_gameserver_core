package com.core.utils;

/**
 * 工具
 * @author King
 *
 */
public class Utils
{
	/** 用默认的类加载器加载一个指定名称的对象 */
	public static Object loadObject(String name)
	{
		return loadObject(name, null);
	}
	
	/** 用指定的类加载器加载一个指定名称的对象 */
	public static Object loadObject(String name, ClassLoader loader)
	{
		try
		{
			Class c = (loader != null) ? loader.loadClass(name) : Class
					.forName(name);
			return c.newInstance();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}
}
