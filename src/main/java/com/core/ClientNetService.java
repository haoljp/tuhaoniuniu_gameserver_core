package com.core;

import java.net.InetSocketAddress;

/**
 * 客户端网络服务器接口
 * @author King
 *
 */
public class ClientNetService extends EnginService
{
	
	/**监听**/
	private NetConnector connector;
	
	public void init(InetSocketAddress address)
	{
		asyncService.init(this.executor, Runtime.getRuntime().availableProcessors()/2);
		connector = new NetConnector(this, address);
	}
	
//	public static void main(String[] args)
//	{
//		new ClientNetService().init(new InetSocketAddress("0.0.0.0", 8080));
//	}
	
	
	
	@Override
	public void shutdown() {
		super.shutdown();
		if(this.connector!=null)
			this.connector.shutdown();
	}

	public NetConnector getConnector() {
		return connector;
	}
	
}
