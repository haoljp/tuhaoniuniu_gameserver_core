package com.core.coder;

public class Constant {

	public static final String TEAM = "tuhaoyouxi";

	public static final String GAME_NAME = "土豪牛牛";

	public static final String AREA_NAME = "重庆服务器1";

	public static final String RSA_PRIVATE_KEY = 
			"MIICeAIBADANBgkqhkiG9w0BAQEFAASCAmIwggJeAgEAAoGBAJ0vsR82C3EDdpjvzPSExp+QaVU2\r\n" +
			"EcmP9filIaqmGjJ+hkQ2SLQx/MhcceoZETN32nXDLXxVATQ6kJwhsYOt9NA+xs43PScrbC6qjwb6\r\n" +
			"YSCw9v6FAV5EztHvedbzIBcynWsPiT1+A9vbjFpEmUXY1RNct3V+BosrHJdOAfUPq3glAgMBAAEC\r\n" +
			"gYBqFwtb/nUgdO7x8EAL9SW+ApElYxYETfb9OC8N54gc9oqWtXDCtk5fyQ1VrE+Qxl8bRn2HwjPK\r\n" +
			"1ZBO4OrX7mwhpe4stHCfFA7kgJtedVqRIOszOA7XAm6FNUkZms6lHTlbw62ERo/U5Mzm69vV9uym\r\n" +
			"TF6ZMQBv7gPpmTmAovJTIQJBANkAFcfGxVg4tmGJ70Z7aOZcTPMSoa8DSILm/SOonnGXtvzc4JxH\r\n" +
			"9pOWFQojUvmfgElxMnW2YhgCl5tT8QvuYu0CQQC5b6M1fhsBOr9Ri7FhdyB/JnkuPHUCVlLvg8rT\r\n" +
			"uBWM9e60zECdcMDhMCosS1SRHsydZn1H0OwB+ib6J7vvHisZAkEAsGqBSokupyp0G5goDwUFo0Im\r\n" +
			"zPa4u6/Po5xm8DaFmZPeEHUBpgxbSmEZecR+ELK7ePmuLES6OwrgdkcoEKsdgQJBAIvNV62fmjpu\r\n" +
			"EQkomR2TE2PpHJk4KTsO284JfWPrOAwi6clS6DAKbtAXYOQuEt6p6reMKk13fEbtUm9sf2qCQxkC\r\n" +
			"QQCdD987Scl7X7aAvf9npFkopZWVo3DwzLp81PchP5fsr8hh2dFgc4hB7kBjAkvSoZ4Z3J0CBI/R\r\n" +
			"PEIzpxLhMPSK";

}
