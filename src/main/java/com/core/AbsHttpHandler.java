package com.core;

import org.apache.log4j.Logger;

import com.core.initialize.HttpMessage;
import com.core.initialize.Message;
import com.core.interfaces.IHandler;

/**
 * 抽象的http服务处理类
 * @author King
 *
 */
public abstract class AbsHttpHandler implements IHandler
{
	private static final Logger log = Logger.getLogger(AbsHttpHandler.class);
	@Override
	public void handler(Message msg) throws Throwable {
		if(!(msg instanceof HttpMessage))
		{
			log.error("发来的不是http消息");
			return;
		}
		handler((HttpMessage)msg);
	}
	
	protected abstract void handler(HttpMessage msg);
}
