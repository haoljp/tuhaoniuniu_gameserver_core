package com.core.config;

/**
 * 资源加载
 * @author  jinmiao
 *
 */
public interface ILoader 
{
	public void load();

}
