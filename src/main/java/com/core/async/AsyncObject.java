package com.core.async;

public abstract class AsyncObject implements Runnable{

	@Override
	public void run() {
		try {
			doCallBack();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 异步处理完后回主线程所做事情
	 * execute callback
	 */
	public abstract void doCallBack() throws Exception;

}
