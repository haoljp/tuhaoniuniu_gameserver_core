package com.core;

import io.netty.channel.Channel;
import io.netty.util.Attribute;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

/**
 * channel管理类
 * @author King
 *
 */
public class ChannelManager
{	
	private static final Logger log = Logger.getLogger(ChannelManager.class);
	/**channel**/
	public Map<Long, NetChannel> channelMap = new ConcurrentHashMap<Long, NetChannel>();
	
	
	
	/**
	 * 添加一个连接
	 * @param channel
	 */
	public void addChannel(Channel channel)
	{
		NetChannel netChannel = new NetChannel(channel);
		this.channelMap.put(netChannel.getChannelId(), netChannel);
	}
	
	/**
	 * 删除一个连接
	 * @param channel
	 */
	public void delChannel(Channel channel)
	{
		Attribute<Long> attribute = channel.attr(ServerAttributeKey.channel_Id);
		if(attribute==null)
		{
			log.error("删除一个连接没有创建id");
			return;
		}
		this.channelMap.remove(attribute.get());
		System.out.println("删除一个连接");
	}
}
