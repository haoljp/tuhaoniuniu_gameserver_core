package com.core;

import io.netty.util.AttributeKey;

public class ServerAttributeKey
{

	public static final AttributeKey<Long> channel_Id = new AttributeKey<Long>("channelId");
	
	public static final AttributeKey<NetChannel> netChannel = new AttributeKey<NetChannel>("netChannel");
	
}
